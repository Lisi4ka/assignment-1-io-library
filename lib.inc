section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    ; Завершаем текущий процесс с использованием системного вызова `exit`
    mov rax, 60
    syscall
    ret 
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov r8, rdi
    mov r9, rdi
next:
    cmp byte [r9],0
    jz finish
    inc r9
    jmp next
finish: 
    sub r9, r8    
    mov rax, r9
    ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:

; Сохраняем адрес строки в регистре RSI
    mov rsi, rdi

    ; Вычисляем длину строки
    mov rcx, 0
loop_start:
    cmp byte [rsi+rcx], 0
    je loop_end
    inc rcx
    jmp loop_start
loop_end:

    ; Выводим строку на экран
    mov rax, 1
    mov rdi, 1
    mov rdx, rcx
    syscall
    ret    
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    ; Устанавливаем значение кода символа новой строки в регистре AL
    push rdx
    push rsi
    push rdi
    
    mov rax, 0xA
    push rax
    
    ; Выводим символ на экран с использованием системного вызова
    mov rax, 1
    mov rdx, 1
    mov rsi, rsp
    mov rdi, 1
    syscall
    pop rax
    xor rax, rax
    pop rdi
    pop rsi
    pop rdx
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12
    push rbx
    ; Получаем беззнаковое 8-байтовое число из аргументов командной строки
    ;mov rdi, rax    ; Выделяем место в стеке для хранения результатов деления
    sub rsp, 32

    ; Инициализируем счетчик разрядов
    mov rcx, 0
    mov rax, rdi
divide_loop:
    ; Делим число на 10
    
    xor rdx, rdx
    mov rbx, 10
    div rbx

    ; Сохраняем остаток от деления в стеке
    add dl, 48 ; Переводим остаток в код ASCII символа цифры
    push rdx
stop_p:
    ; Увеличиваем счетчик разрядов
    inc rcx

    ; Проверяем, дошли ли мы до конца числа
    test rax, rax
    jnz divide_loop
    mov r12, rcx
print_loop:
    ; Выводим цифры числа из стека на экран
    dec r12
    
    ;mov rax, 1
    ;mov rbx, 1
    ;mov rsi, rsp
    ;mov rdx, 1
    ;syscall
    pop rdi
    call print_char
    ; Проверяем, были ли выведены все цифры числа
    test r12, r12
    jnz print_loop

    ; Освобождаем выделенное место в стеке
    add rsp, 32

    ; Завершаем программу
    pop rbx
    pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov r9, rdi
    shr rdi, 63
    cmp rdi, 1
    mov rdi, r9
    je negat
    call print_uint
    ret
negat: 
    mov r8, rdi
    neg r8
    push r8
    mov rdi, 2dh
    call print_char
    pop r8
    mov rdi, r8
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:


; Используемся регистр RCX для перебора символов строк
    xor r9, r9    ; индекс символа

compare_loop:
    ; Загружаем символы из строк
    mov r11b, byte [rdi+r9]
    mov r10b, byte [rsi+r9]

    ; Проверяем, равны ли символы
    cmp r11b, r10b
    jne not_equal

    ; Проверяем, достигнут ли конец обеих строк
    cmp r11b, 0
    je equal

    ; Увеличиваем индекс символа
    inc r9

    ; Переходим к следующему символу
    jmp compare_loop

equal:
    ; Строки равны, возвращаем 1
    xor rax, rax
    mov rax, 1
    
    ret

not_equal:
    ; Строки не равны, возвращаем 0
    xor rax, rax
    ret
    
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    ; Выделяем место на стеке для хранения результата
    push rdi 
    push rdi

    ; Вызываем системный вызов read для чтения одного символа из stdin
    mov rax, 0                       ; номер системного вызова read
    mov rdi, 0                       ; файловый дескриптор stdin
    mov rsi, rsp                     ; адрес буфера для чтения
    mov rdx, 1                       ; количество байт для чтения
    syscall
    jc _error

    ; Если результат системного вызова read равен 0, то достигнут конец потока
    cmp rax, 0
    je end

    ; Загружаем прочитанный символ из стека в регистр RAX
    xor rax, rax
    mov al, byte [rsp]

end:
    ; Освобождаем выделенное место на стеке
    pop rdi
    pop rdi

    ; Возвращаем результат
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    
    push r12
    push r13
    push r14
    mov r14, 0
; Сохраняем адрес начала буфера в регистр RDI
    mov rdi, rdi
    
    ; Сохраняем размер буфера в регистр RSI
    mov rsi, rsi
    
    ; Инициализируем счетчик длины слова
    xor r9, r9
    mov r10, rsi
    mov r13, rdi
    push r15
    push r15
read_loop:
    
    ; Вызываем системный вызов read для чтения одного символа из stdin
    
    mov rax, 0  
                     ; номер системного вызова read
    mov rdi, 0                       ; файловый дескриптор stdin
    mov rdx, 1 
    mov rsi, rsp 
br:                     ; количество байт для чтения
    syscall
    
    
    ; Если символ является пробельным (пробел, табуляция или перевод строки),
    ; пропускаем его и продолжаем считывать следующие символы
    mov r8, r13
    add r8, r9
    cmp rax, 0
    je success
    cmp rcx, 4
    je success
    cmp byte [rsp], 20h
    je looop
    cmp byte [rsp], 9
    je looop
    cmp byte [rsp], 0Ah
    je success
    cmp byte [rsp], 0
    je success
    cmp byte [rsp], 0Dh
    je success
    cmp byte [rsp], 40h
    je success


    mov r14, 1
    ; Увеличиваем счетчик длины слова
    inc r9
    
    ; Проверяем, достигнут ли размер буфера
    cmp r9, r10
    ja too_long
    
    ; Копируем считанный символ в буфер
    mov r12b, byte [rsp]
    mov byte [r8], r12b
    
    ; Увеличиваем указатель на буфер и адрес начала буфера
    ;inc rdi
    
    ; Возвращаемся к началу цикла чтения символов
    jmp read_loop
looop:
    cmp r14, 0
    je read_loop
    jmp success    

too_long:
    ; В случае, если слово слишком большое для буфера,
    ; возвращаем 0 в регистре RAX
    xor rax, rax
    pop r15
    pop r15
    pop r14
    pop r13
    pop r12

    ret

success:
    
    ; Дописываем нуль-терминатор к слову в буфере
    mov byte [r8], 0
    
    ; Возвращаем адрес буфера в регистре RAX
    mov rax, r13
    
    ; Возвращаем длину слова в регистре RDX
    mov rdx, r9
    
    ; Возвращаемся из функции
    pop r15
    pop r15
    pop r14
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
dec rsp
    xor r9, r9
    xor r10, r10

read_loopp:
    cmp byte [rdi + r10], 0Ah
    je succes
    xor r8, r8
    mov r8b, byte [rdi + r10]
    cmp r8, 48
    jl fail
    cmp r8, 57
    jg fail
    sub r8, 48
    mov rax, 10
    mul r9
    mov r9, rax
    add r9, r8
    inc r10
    jmp read_loopp

fail:
    cmp r10, 0
    jne succes
    mov rdx, 0
    mov rax, 0
    inc rsp
    ret
succes:
    mov rdx, r10
    mov rax, r9

    inc rsp
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], 2dh
    jne positt
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
positt:
    call parse_uint
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:

copy_string_to_buffer:
    ; Сохраняем указатели на строку и буфер в регистрах RDI и RSI
    mov rdi, rdi    ; указатель на строку
    mov rsi, rsi    ; указатель на буфер

    ; Сохраняем длину буфера в регистре RDX
    mov rdx, rdx    ; длина буфера

    ; Инициализируем счетчик для длины строки
    xor rcx, rcx    ; длина строки

    ; Копируем символы из строки в буфер
copy_loop:
    ; Загружаем символ из строки
    mov al, byte [rdi+rcx]

    ; Проверяем, достигли ли конца строки или буфера
    cmp al, 0
    mov byte [rsi+rcx], al
    je done
    cmp rcx, rdx
    jae done

    ; Копируем символ из строки в буфер
    mov byte [rsi+rcx], al

    ; Увеличиваем счетчик для длины строки
    inc rcx

    ; Переходим к следующему символу
    jmp copy_loop

done:
    
    ; Если длина строки не превысила размер буфера, возвращаем длину строки
    cmp rcx, rdx
    jb successss

    ; Иначе, возвращаем 0 - строка не помещается в буфер
    xor rax, rax
    ret
successss:
    mov rax, rcx
    ; Возвращаем длину строки в rcx
    ret

err db `Critical error while executing syscall!\0`
_error:
    mov rdi, err
    call print_string
    call exit
    ret
